

# Welcome to AC Back-end!
În primul și primul rând am vrea să începem prin a vă mulțumi pentru faptul că sunteți astăzi aici și să vă felicităm pentru toată munca pe care ați depus-o până acum în cadrul întregului proces de recrutare. **Well done!**

![enter image description here](https://media.giphy.com/media/MOWPkhRAUbR7i/giphy.gif)

Acum, pentru ceea ce urmează, vă îndemnăm să vă încărcați cu și mai multă energie și bună dispoziție și vă promitem că fiecare moment și fiecare minut dedicat **SiSC**-ului va merita din plin.
**Și pentru că suntem IT și ne place eficiența, să ne focusăm asupra acestui aspect acum.**

![](https://lh5.googleusercontent.com/qweNk3Go8pfwImBqBgmuCJstnD4BnJfOEtjzS_cLBg6oVs3BlggYMBMY8pGDuFceJtIQNez8Hb-gsaO3-b1Hf_ey4sMHwv0Lr_CyXRD4FLyk7bxd2Umu6iROZChkszbnGkEptE2I)

Pentru început, după cum probabil ați realizat, **Git** este o unealtă de bază pentru noi. Aici ajung toate proiectele noastre și de aici pleacă mai departe toate creațiile către publicul larg.
Pentru că ne oferă posibilitatea să menținem și lucrăm întotdeauna pe ultima versiune a fișierelor, acesta aduce un avantaj enorm unei **echipe** care colaborează constant, deci o să vă rugăm și pe voi să îi acordați la fel de multă importanță și să fiți atenți în momentul în care vom explica funcționalitățile de bază.
![enter image description here](https://media.giphy.com/media/3ohuAxV0DfcLTxVh6w/giphy.gif)
  
Puțin mai jos vom lista și **necesarele + materialele** pe care vi le-am pus la dispoziție acum câteva zile, în cazul în care va fi nevoie să le accesați ceva mai rapid:
*Tutoriale:*
 - [**JavaScript basics**](https://www.codecademy.com/learn/introduction-to-javascript)
 - **[SQL basics](https://www.codecademy.com/learn/learn-sql)**
 - **[NodeJS basics](https://www.youtube.com/watch?v=U8XF6AFGqlc)**
  
 *Programe de instalat:*
  
 - **[Xampp](https://www.apachefriends.org/ro/download.html)**
 ```Precizare: nu îl instalați în C:\```
 - **[Visual Studio Code](https://code.visualstudio.com/)**
 - **[NodeJS](https://nodejs.org/en/)**
 - **[Git](https://git-scm.com/downloads)**

![enter image description here](https://media.giphy.com/media/2juvZoQ3oLa4U/giphy.gif)

# Acum, să trecem la treabă!
## **Ce veți avea de făcut mai exact?**
Prima dată va trebui să facem rost de materialele cu care vom lucra. Pentru a face asta deschidem:
-   **Linia de comandă** - dacă avem Ubuntu
-   **[Git bash](https://git-scm.com/download/win)** - dacă avem Windows
Vom crea un folder, iar aici vom face practic **o clonă** la tot ceea ce v-am pregătit, folosind comanda:

`git clone REPO_HTTPS_ADDRESS`

În cazul nostru:

`git clone https://gitlab.com/itsisc/ac-backend-2019.git`

Acum, dacă navigăm în folderul de mai sus...

![enter image description here](https://user-content.gitlab-static.net/b108b19430b1df98d9389bbd3133b0941e1af5e8/68747470733a2f2f692e67697068792e636f6d2f6d656469612f724b7a784530513545514b46712f67697068792e77656270)

...vom găsi un sub-folder nou, cu toate fișierele din acest **repository**.
Mai mult sau mai puțin, acolo se regăsesc **2 formulare**, iar pentru fiecare vor exista anumite **cerințe**, de care va trebui să ne ocupăm astăzi:
# Dubla I: Acțiune
![enter image description here](https://media.giphy.com/media/1zi6nr3YjLple/giphy.gif)

Acest formular este puțin mai special deoarece proprietarii cinema-ului au avut o mică dispută cu divizia IT, iar aceștia au făcut o mică glumă stricându-le formularul de achiziționare a biletelor. Voi, protagoniștii acestui film, va trebui să-i ajutați să repare formularul.
## Ce veți avea de făcut?
- Accesați folderul tocmai clonat, apoi accesați **formular1**
 - În fereastra de bash rulați comanda:
 `code.`
 O să se deschidă Visual Studio Code automat unde veți găsi formularul cu probleme.
  
 - Rulați modulele **Apache** și **MySQL** din XAMPP Control Panel
 - Se crează baza de date **siscit_backend** cu **Utf8_general_ci la Collation**, nu vă speriați este extrem de simplu și vă vom arăta cum să faceți asta ;)
 - Deschidem terminalul în Visual Studio Code și rulăm următoarele comenzi: 
 
	`cd ./back-end`
  
	`npm install` 
  
	 `npm install -g nodemon`
- Pentru a porni formularul folosiți comanda: `nodemon`
- Din Visual Studio Code, accesați fișierul **server.js** și analizați în terminal erorile strecurate de ștrengarii diviziei IT astfel încât cinematograful să poată funcționa din nou.
- Atunci când în terminal nu se va mai afla nicio eroare, accesați în browserul vostru **localhost:8080**
- În momentul în care datele clienților vor fi introduse fără erori în tabela **cinema_clients** din baza de date **siscit_back_end**, proprietarii cinematografului vor fi recunoscători.
> Pentru testare încercați să aveți deschise în paralel formularul din browser și terminalul din Studio Code, iar de fiecare dată când faceți modificări să îl reîncărcați. De asemenea, după modificări, nu uitați să salvați! **Ctrl + S**
> 
> În plus, pentru a nu pierde timpul, puteți prioritiza funcționalitatea, nu validitatea datelor (ex. adresa **ion@aaa.com** e la fel de bună precum **ionut.popescu@gmail.com** sau **0711222333** e la fel de ok ca **numărul vostru real de telefon**).

**Ai îndeplinit cu succes toți pașii?**

![enter image description here](https://media.giphy.com/media/sBLcw5Ic4QUTK/giphy.gif)

**Felicitări!** Atunci hai să îi anunțăm și pe ceilalți că problema a fost rezolvată și să urcăm codul sursă undeva... Evident, pe Git.
-   Mergem pe profilul nostru de **Gitlab** și dăm click pe butonul mare și verde cu **New Project**.
  
 -   Numele proiectului va fi **ac back 2019**
 -   Nivelul de vizibilitate va fi **Public**
 -   Și vom bifa căsuța pentru **README.md**, pentru a-l clona mai ușor pe local
 -   *Opțional, puteți adăuga o descriere scurtă și amuzantă
  
> Important de menționat că ori de câte ori veți face modificări și veți vrea să le urcați pe Git va trebui să urmați aceiași pași. Deci atenție, pentru că nu e nimic dificil și merită din plin.
-   Pentru a verifica dacă există diferențe între repository și folderul local rulăm:
  
 ```
 git status 
 ```
  
-   Pentru a adăuga un fișier modificat din listă avem:
  
 ```
 git add FILE_NAME
 ```
  
-   În caz că vrem să le adăugăm pe **toate**:
  
 ```
 git add .
 ```
  
-   Acum putem rula din nou **git status**, să vedem ce fișiere au fost adăugate cu succes.
  
-   Iar pentru ca toate modificările să aibă sens și toți ceilalți colaboratori să înțeleagă care a fost scopul update-ului, trebuie să oferim și un mesaj specific:
  
 ```
 git commit -m "YOUR_MESSAGE_HERE"
  
 ex. -->  git commit -m "Fixed ERRORS"
 ```
  
-   În final vom folosi o super-putere ce va trimite toată informația către proiectul pe care tocmai ce l-ați creat. Sună cam așa:
  
 ```
 git push
 ```
![enter image description here](https://media.giphy.com/media/c0NwRD0Vi5Cta/giphy.gif)

# Dubla II
Ei bine, aici nu este nimic stricat, dar asta nu înseamnă că toate lucrurile sunt la locul lor. Departamentul IT Netflix a întâmpinat câteva probleme în realizarea formularului pentru înregistrarea abonamentelor. Acestui formular îi lipsesc validările, ceea ce înseamnă că avem nevoie de voi să ne ajutați să-l securizăm. Așadar...

**1.Aplicați formularului următoarele validări**

a) câmpuri non-empty: nume, prenume, telefon, email, facebook, tipAbonament, nrCard, cvv;

b) câmpuri cu limită de caractere: nume, prenume (minim 3 caractere, maxim 20);

c) câmpuri ce permit doar litere: nume, prenume ;

d) validare pentru numărul de telefon ( lungime = 10, doar numere);

e) validare pentru email (formă specifică [exemplu@zzz.yyy](mailto:exemplu@zzz.yyy));

f) validarea link-ului de facebook;

g) validare pentru nrCard (lungime = 16, doar numere);

h) validare pentru cvv (lungime = 3, doar numere);

**Done?**

![enter image description here](https://media.giphy.com/media/ytTYwIlbD1FBu/giphy.gif)

Acum că suntem "protejați", să îl **dezvoltăm** puțin:

**2. Adăugați câmpul vârstă în formular.**
 -   Acesta trebuie să existe atât în partea de front-end, cât și în baza de date, cu tipul VARCHAR, de 3 de caractere.
 -   Aplicați acestuia următoarele validări: **non-empty, doar cifre, lungime minimă un caracter, lungime maximă 3 caractere.**
  
 ![enter image description here](https://media.giphy.com/media/8xomIW1DRelmo/giphy.gif)
 
**
**3.Adăugați câmpul CNP în formular.****
 - Acesta trebuie să existe atât în partea de front-end, cât și în baza de date, cu tipul VARCHAR, de 20 de caractere.
 -   Aplicați acestuia următoarele validări: non-empty, doar cifre, lungime = 13
 
**4. Asigurați-vă de unicitatea câmpului email în baza de date.**

![enter image description here](https://media.giphy.com/media/U2Al0EC7dhB366mJCz/giphy.gif)

**5. Analizați data de naștere și CNP-ul.**
 -   Permiteți înscrierea unui participant doar dacă aceste 2 câmpuri sunt compatibile.

![enter image description here](https://media.giphy.com/media/Nzyu1yTNdtDws/giphy.gif)

**6.Adăugați câmpul "sex" în formular.**
 - Acesta trebuie să apară doar în baza de date și să fie creat pe baza CNP-ului.
> **Notă**: Dacă prima cifră a CNP-ului este **1, 3 sau 5** în baza de date se va introduce litera "**M**", iar dacă prima cifră a CNP-ului este **2, 4 sau 6** în baza de date se va introduce litera "**F**"
Dacă ai ajuns până aici cu bine, atunci **FELICITĂRI...**

![enter image description here](https://media.giphy.com/media/VFB3cJJne7b5m/giphy.gif)

Iar pentru că un super-erou își ține întotdeauna codul pe **Git**...
```
push push push!
```